﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CSCore;
using CSCore.Codecs;
using CSCore.CoreAudioAPI;
using CSCore.SoundOut;
using CMP;
using System.Threading;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace CMPMediaPlayer
{
    public partial class MainWindow : Window, IDisposable
    {
        LibraryPage songListComponent = new LibraryPage();
        TagEditPage editPage;

        private static MusicPlayer xMusicPlayer = new MusicPlayer();
        private bool volumeMuted = false;
        private System.Windows.Threading.DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
        private BitmapImage defaultCover;

        public MainWindow()
        {
            InitializeComponent();
            InitializeDefaultCover();

            //SongList ClickEvent to Edit Tag
            songListComponent.ccontextEdit.Click += TagEditEventListener;
            songListComponent.ccontextPlay.Click += ListViewSongsContextMenuPlay;
            songListComponent.songListView.MouseDoubleClick += ListViewSongsMouseDoubleClick;
            songListComponent.songListView.PreviewKeyDown += ListViewSongsEnterPreview;

            //GUI
            workbench.Content = songListComponent;

            //Ticker
            timer.Interval = new TimeSpan(0, 0, 0, 0, 200);
            timer.Tick += new EventHandler(Timer_SliderEvent);
        }

        private void InitializeDefaultCover()
        {
            defaultCover = new BitmapImage(new Uri(@"Images/cover.jpg", UriKind.RelativeOrAbsolute));
        }

        private void SetTitle()
        {
            titleLabel.Content = xMusicPlayer.CurrentTitle;
        }

        private void SetInterpret()
        {
            interpretLabel.Content = xMusicPlayer.CurrentArtist;
        }

        private void PlaySong(string url = null, bool force = false)
        {
            if (force || !xMusicPlayer.SourceLoaded || url != null)
            {
                try
                {
                    if (url != null)
                        xMusicPlayer.OpenFile(url);
                    else if (songListComponent.songListView.SelectedItems.Count > 0)
                        xMusicPlayer.OpenFile(((Song)(songListComponent.songListView.SelectedItem)).URL);
                    else
                        return;
                }
                catch { return; }
            }

            TogglePlayPause();

            new Thread(() =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    TimeSlider.IsEnabled = true;
                    SetTotalTime();
                    SetTitle();
                    SetInterpret();
                    ShowAlbumCover();
                }));
            }).Start();
        }

        private void TogglePlayPause()
        {
            if (xMusicPlayer.PlayerState == PlaybackState.Playing)
            {
                xMusicPlayer.Pause();
                PlayButtonIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.PlayCircleOutline;
                timer.Stop();
            }

            else if (xMusicPlayer.SourceLoaded)
            {
                xMusicPlayer.Play();
                PlayButtonIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.PauseCircleOutline;
                timer.Start();
            }
        }

        //Zeige Albumcover an
        private void ShowAlbumCover()
        {
            try
            {
                if (xMusicPlayer.CurrentAlbumCover != null)
                {
                    //Hole Coverdaten
                    MemoryStream ms = new MemoryStream(xMusicPlayer.CurrentAlbumCover.Data.Data);
                    ms.Seek(0, SeekOrigin.Begin);

                    //Erzeuge Bitmap
                    BitmapImage bitmap = new BitmapImage();
                    //Bitmap bmp = new Bitmap();
                    bitmap.BeginInit();
                    bitmap.StreamSource = ms;
                    bitmap.EndInit();

                    //Zeige Cover an
                    imageBox.Source = bitmap;
                }
                else
                {
                    //Zeige DefaultCover an, wenn kein Cover gefunden wurde
                    imageBox.Source = defaultCover;
                }
            }
            catch
            {
                MessageBox.Show("WTF geat iatz");
            }
        }

        //Setze die Gesamtzeit des Songs
        public void SetTotalTime()
        {
            TotalTimeLabel.Content = String.Format(@"{0:mm\:ss}", xMusicPlayer.TotalLength);
            TimeSlider.Maximum = xMusicPlayer.TotalLength.TotalSeconds;
        }

        //SliderEvent
        private void Timer_SliderEvent(object sender, EventArgs e)
        {
            TimeSpan totallength = xMusicPlayer.TotalLength;
            TimeSpan position = xMusicPlayer.CurrentPosition;

            //Position can't be bigger than length
            if (totallength < position)
                totallength = position;

            CurrentTimeLabel.Content = String.Format(@"{0:mm\:ss}", position, totallength);
            TimeSlider.Value = position.TotalSeconds;
        }

        private void TimeSlider_DragStarted(object sender, System.Windows.Controls.Primitives.DragStartedEventArgs e)
        {
            timer.Stop();
            TimeSlider.ValueChanged -= TimeSlider_ValueChanged;
        }

        private void TimeSlider_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            xMusicPlayer.CurrentPosition = TimeSpan.FromSeconds(TimeSlider.Value);
            if (xMusicPlayer.PlayerState == PlaybackState.Playing)
            {
                xMusicPlayer.Play();
                PlayButtonIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.PauseCircleOutline;
                timer.Start();
            }

            TimeSlider.ValueChanged += TimeSlider_ValueChanged;
        }

        private void TimeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (e.NewValue - 2 > e.OldValue || e.NewValue < e.OldValue)
            {
                timer.Stop();
                TimeSlider.Value = e.NewValue;
                xMusicPlayer.CurrentPosition = TimeSpan.FromSeconds(TimeSlider.Value);

                if (xMusicPlayer.PlayerState == PlaybackState.Playing)
                {
                    timer.Start();
                }
            }

            if (TimeSlider.Value == TimeSlider.Maximum)
            {
                timer.Stop();
                TimeSlider.Value = 0;
                timer.Start();
            }
        }

        private void VolumeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!volumeMuted)
            {
                double newVolume = VolumeSlider.Value;
                double oldVolume = e.OldValue;

                xMusicPlayer.Volume = Convert.ToInt32(newVolume);
                if (oldVolume == 0 && newVolume > 0)
                    VolumeButtonIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.VolumeHigh;
                else if (newVolume == 0)
                    VolumeButtonIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.VolumeMute;
            }
        }

        private void PlayButtonClick(object sender, RoutedEventArgs e)
        {
            TogglePlayPause();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            PlayPreviousSong();
        }

        private void VolumeButtonClick(object sender, RoutedEventArgs e)
        {
            if (!volumeMuted)
            {
                xMusicPlayer.Volume = 0;
                VolumeButtonIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.VolumeOff;
                volumeMuted = true;
            }
            else
            {
                xMusicPlayer.Volume = Convert.ToInt32(VolumeSlider.Value);

                if (VolumeSlider.Value > 0)
                    VolumeButtonIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.VolumeHigh;
                else
                    VolumeButtonIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.VolumeMute;

                volumeMuted = false;
            }
        }

        private void PlayPreviousSong()
        {
            try
            {
                if (songListComponent.songListView.SelectedIndex > 0)
                {
                    songListComponent.songListView.SelectedIndex--;
                    PlaySong(force: true);
                }
            }
            catch { }
        }

        private void ForwardButton_Click(object sender, RoutedEventArgs e)
        {
            PlayNextSong();
        }

        private void PlayNextSong()
        {
            if (songListComponent.songListView.SelectedIndex < songListComponent.songListView.Items.Count - 1)
            {
                songListComponent.songListView.SelectedIndex++;
                PlaySong(force: true);
            }
        }

        private void VolumeDownButton_Click(object sender, RoutedEventArgs e)
        {
            //xMusicPlayer.Volume -= 10;
        }

        private void VolumeUpButton_Click(object sender, RoutedEventArgs e)
        {
            //xMusicPlayer.Volume += 10;
        }

        private void TagEditEventListener(object sender, RoutedEventArgs e)
        {
            try
            {
                editPage = new TagEditPage(GetFileURLFromSongList());
                editPage.cancelButton.Click += TagEditCancelEvent;
                editPage.saveButton.Click += TagEditSaveEvent;
                workbench.Content = editPage;
            }
            catch
            {
                MessageBox.Show("Cannot edit tags! file is in use: ");
            }
        }

        private void TagEditCancelEvent(object sender, RoutedEventArgs e)
        {
            ShowSongList();
        }

        private void TagEditSaveEvent(object sender, RoutedEventArgs e)
        {
            try
            {
                //Save new metadata
                TagLib.File metaFile = TagLib.File.Create(GetFileURLFromSongList());
                metaFile.Tag.Title = editPage.titleBox.Text;
                metaFile.Tag.Performers = new String[] { editPage.artistBox.Text };
                metaFile.Save();
                ShowSongList();
            }
            catch (Exception ex) { MessageBox.Show("cannot write metadata: " + ex); }
        }

        private String GetFileURLFromSongList()
        {
            int index = songListComponent.songListView.SelectedIndex;
            return songListComponent.songList[index].URL ?? "";
        }

        private void Menu_PlayerClick(object sender, RoutedEventArgs e)
        {
            ShowSongList();
        }

        private void Menu_AudioClick(object sender, RoutedEventArgs e)
        {
            workbench.Content = new AudioDevicePage();
        }

        private void OKButton_ClickDone(object sender, RoutedEventArgs e)
        {
            ShowSongList();
        }

        private void Menu_DirectoryClick(object sender, RoutedEventArgs e)
        {
            workbench.Content = new MusicFoldersPage();
        }

        public void Dispose()
        {
            songListComponent.ShutDown();
            Environment.Exit(0);
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            this.Dispose();
        }

        public void ShowSongList()
        {
            try
            {
                songListComponent.RescanSongs();
                songListComponent.RefreshAudioDevice();
                workbench.Content = songListComponent;
            }
            catch(Exception exs) { MessageBox.Show(exs.ToString()); }
        }

        private void menuOpenButton_Click(object sender, RoutedEventArgs e)
        {
            menuOpenButton.Visibility = Visibility.Collapsed;
            menuCloseButton.Visibility = Visibility.Visible;
        }

        private void menuCloseButton_Click(object sender, RoutedEventArgs e)
        {
            menuOpenButton.Visibility = Visibility.Visible;
            menuCloseButton.Visibility = Visibility.Collapsed;
        }

        private void menuList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (menuList.SelectedItem.Equals(menuPlayerItem))
                ShowSongList();
            else if (menuList.SelectedItem.Equals(menuFolderItem))
                workbench.Content = new MusicFoldersPage();
            else if (menuList.SelectedItem.Equals(menuSpeakerItem))
                workbench.Content = new AudioDevicePage();
        }

        //SongList events

        private void ListViewSongsMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PlaySongOnEvent();
        }


        private void ListViewSongsContextMenuPlay(object sender, RoutedEventArgs e)
        {
            PlaySongOnEvent();
        }

        private void ListViewSongsEnterPreview(object sender, KeyEventArgs e)
        {
            var uiElement = e.OriginalSource as UIElement;

            if (e.Key == Key.Enter && uiElement != null)
            {
                e.Handled = true;
                PlaySongOnEvent();
            }
        }

        private void PlaySongOnEvent()
        {
            try
            {
                int index = songListComponent.songListView.SelectedIndex;
                String songToPlay = songListComponent.songList[index].URL;
                PlaySong(songToPlay);
            }
            catch (Exception e)
            {
                MessageBox.Show("Song is not available: " + e);
            }
        }
    }
}