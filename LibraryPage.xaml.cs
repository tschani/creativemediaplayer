﻿using CSCore.Codecs;
using CSCore.SoundOut;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CMP
{
    public partial class LibraryPage : UserControl
    {
        public static Dictionary<String, FileSystemWatcher> directoryWatcher = new Dictionary<String, FileSystemWatcher>();
        private static MusicPlayer xMusicPlayer = new MusicPlayer();
        private System.Windows.Threading.DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
        public ObservableCollection<Song> songList;

        public LibraryPage()
        {
            InitializeComponent();
            AddDirectoryListeners();

            

            //GUI!!!!!!!!!!!!! CHANGE!!!!!!!!!!!!!!
            new Thread(() =>
            {
                SongScanner scanner = new SongScanner();
                songList = scanner.ScanAllSongs();

                songListView.Dispatcher.BeginInvoke((Action)(() =>
                {
                    songListView.ItemsSource = songList;
                }));
            }).Start();
        }

        private void AddDirectoryListeners()
        {
            //Get Music-Directories
            var directories = CMP.Properties.Settings.Default.MusicDirectories;

            if (directories != null)
                foreach (String directory in directories)
                {
                    try
                    {
                        //DIRECTORY WATCHER
                        directoryWatcher[directory] = new FileSystemWatcher();
                        directoryWatcher[directory].Path = directory;
                        directoryWatcher[directory].Filter = "*.*";
                        directoryWatcher[directory].NotifyFilter = NotifyFilters.Attributes |
                                                        NotifyFilters.CreationTime |
                                                        NotifyFilters.FileName |
                                                        NotifyFilters.LastAccess |
                                                        NotifyFilters.LastWrite |
                                                        NotifyFilters.Size |
                                                        NotifyFilters.Security;
                        directoryWatcher[directory].Deleted += new FileSystemEventHandler(SongDeletedEvent);
                        directoryWatcher[directory].Created += new FileSystemEventHandler(SongAddedEvent);
                        directoryWatcher[directory].EnableRaisingEvents = true;
                        directoryWatcher[directory].IncludeSubdirectories = true;
                    }
                    catch { }
                }
        }

        public void ShutDown()
        {
            timer.Stop();
            xMusicPlayer.Stop();
        }

        private void SongAddedEvent(object sender, FileSystemEventArgs e)
        {
            AddSong(e.FullPath);
        }

        private void SongDeletedEvent(object sender, FileSystemEventArgs e)
        {
            RemoveSong(e.FullPath);
        }

        private void RemoveSong(String path)
        {
            FileInfo file = new FileInfo(path);

            songListView.Dispatcher.BeginInvoke((Action)(() =>
            {
                songList.Remove(songList.Single(s => s.URL == path));
            }));
        }

        private void AddSong(String path)
        {
            FileInfo file = new FileInfo(path);
            SongScanner scanner = new SongScanner();

            new Thread(() =>
            {
                var songToAdd = scanner.ScanSingleSong(file.FullName);
                songListView.Dispatcher.BeginInvoke((Action)(() =>
                {
                    songList.Add(songToAdd);
                }));
            }).Start();
        }

        public void RescanSongs()
        {
            new Thread(() =>
            {
                songListView.Dispatcher.BeginInvoke((Action)(() =>
                {
                    songList.Clear();
                }));

                SongScanner scanner = new SongScanner();

                foreach (Song s in scanner.ScanAllSongs())
                {
                    songListView.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        songList.Add(s);
                    }));
                }

                songListView.Dispatcher.BeginInvoke((Action)(() =>
                {
                    songListView.InvalidateVisual();
                }));

                AddDirectoryListeners();
            }).Start();
        }

        public void RefreshAudioDevice()
        {
            xMusicPlayer.SetAudioDevice();
        }
    }
}