﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSCore;
using CSCore.Codecs;
using CSCore.CoreAudioAPI;
using CSCore.SoundOut;
using System.Collections.ObjectModel;
using TagLib;
using System.IO;
using System.Collections.Concurrent;
using System.Threading;

namespace CMP
{
    public partial class MusicPlayer : Component
    {
        private ISoundOut _soundOut;
        private IWaveSource _waveSource;
        public readonly ObservableCollection<MMDevice> Devices = new ObservableCollection<MMDevice>();
        public MMDevice CurrentAudioDevice;
        private string _title, _album, _url, _artist;
        private IPicture _albumCover;

        public bool SourceLoaded
        {
            get
            {
                if (_waveSource == null)
                    return false;
                else
                    return true;
            }
        }

        public PlaybackState PlayerState
        {
            //get playerstate
            get
            {
                if (_soundOut != null)
                    return _soundOut.PlaybackState;
                return PlaybackState.Stopped;
            }
        }

        public String CurrentURL
        {
            //get current song URL
            get
            {
                return _url;
            }

            //set current song URL
            set
            {
                _url = value;
            }
        }

        public TimeSpan CurrentPosition
        {
            //get current song position
            get
            {
                if (_waveSource != null)
                    return _waveSource.GetPosition();
                return TimeSpan.Zero;
            }

            //set current song position
            set
            {
                if (_waveSource != null)
                    _waveSource.SetPosition(value);
            }
        }

        public TimeSpan TotalLength
        {
            //get the length of the current song
            get
            {
                if (_waveSource != null)
                {
                    return _waveSource.GetLength();
                }
                return TimeSpan.Zero;
            }
        }

        public int Volume
        {
            //get volume of the current song
            get
            {
                if (_soundOut != null)
                    return Math.Min(100, Math.Max((int)(_soundOut.Volume * 100), 0));
                return 100;
            }

            //Set volume of the current song
            set
            {
                if (_soundOut != null)
                    _soundOut.Volume = Math.Min(1.0f, Math.Max(value / 100f, 0f));
            }
        }

        //return the current title
        public String CurrentTitle
        {
            get
            {
                return _title;
            }
        }

        //Returns the current albumname
        public String CurrentAlbum
        {
            get
            {
                return _album;
            }
        }

        //Returns the albumcover
        public IPicture CurrentAlbumCover
        {
            get
            {
                return _albumCover;
            }
        }

        //Returns the albumcover
        public String CurrentArtist
        {
            get
            {
                return _artist;
            }
        }

        public MusicPlayer()
        {
            InitializeComponent();

            //read all MMDDevices
            using (var mmdeviceEnumerator = new MMDeviceEnumerator())
            {
                using (
                    var mmdeviceCollection = mmdeviceEnumerator.EnumAudioEndpoints(DataFlow.Render, DeviceState.Active))
                {
                    foreach (var device in mmdeviceCollection)
                    {
                        Devices.Add(device);
                    }
                }
            }

            SetAudioDevice();
        }

        public void SetAudioDevice()
        {
            //set current audiodevice
            try
            {
                CurrentAudioDevice = Devices.Single(d => d.FriendlyName == CMP.Properties.Settings.Default.AudioDevice);
            }
            catch
            {
                foreach (MMDevice device in Devices)
                {
                    try
                    {
                        CurrentAudioDevice = device;
                        Console.WriteLine("====================================\n" + CMP.Properties.Settings.Default.AudioDevice + "====================================\n");
                        CMP.Properties.Settings.Default.AudioDevice = Devices.ElementAt(0).FriendlyName;
                        CMP.Properties.Settings.Default.Save();
                        break;
                    }
                    catch { }
                }
            }
        }

        public MusicPlayer(IContainer container)
        {
            InitializeComponent();
            container.Add(this);
            this.Volume = 100;
        }

        //open audiofile
        public void OpenFile(string filename)
        {
            CleanupPlayback();

            //set song
            _waveSource = CodecFactory.Instance.GetCodec(filename).ToSampleSource().ToMono().ToWaveSource();
            _soundOut = new WasapiOut() { Latency = 100, Device = CurrentAudioDevice };
            _soundOut.Initialize(_waveSource);

            //set ID3Tags
            TagLib.File file = TagLib.File.Create(filename);
            _title = file.Tag.Title ?? file.Name;
            _url = filename;
            _album = file.Tag.Album;

            //AlbumCover
            if (file.Tag.Pictures.Length > 0)
                _albumCover = file.Tag.Pictures[0];
            else
                _albumCover = null;

            //Artist
            if (file.Tag.AlbumArtists.Length > 0)
                _artist = file.Tag.Performers[0];
            else
                _artist = "Unknown Artist";
        }

        //start the song
        public void Play()
        {
            if (_soundOut != null)
                _soundOut.Play();
        }

        //pause the song
        public void Pause()
        {
            if (_soundOut != null)
                _soundOut.Pause();
        }

        //stop the song
        public void Stop()
        {
            if (_soundOut != null)
                _soundOut.Stop();
        }

        //clean playback
        private void CleanupPlayback()
        {
            if (_soundOut != null)
            {
                _soundOut.Dispose();
                _soundOut = null;
            }
            if (_waveSource != null)
            {
                _waveSource.Dispose();
                _waveSource = null;
            }
        }
    }
}
