﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CMP
{
    public partial class AudioDevicePage : UserControl
    {
        private MusicPlayer musicPlayer = new MusicPlayer();

        public AudioDevicePage()
        {
            InitializeComponent();
            try
            {
                foreach (var obj in musicPlayer.Devices)
                    DeviceBox.Items.Add(obj);
            }
            catch { }

            try
            {
                DeviceBox.SelectedItem = musicPlayer.Devices.Single(d => d.FriendlyName == CMP.Properties.Settings.Default.AudioDevice);
            }
            catch { }
        }

        private void DeviceBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                CMP.Properties.Settings.Default.AudioDevice = DeviceBox.SelectedValue.ToString();
                CMP.Properties.Settings.Default.Save();
            }
            catch { }
        }
    }
}