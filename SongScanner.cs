﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMP
{
    class SongScanner
    {
        public Song ScanSingleSong(String fileName)
        {
            try
            {
                TagLib.File fileID = TagLib.File.Create(fileName);
                Song song = new Song();

                song.Title = fileID.Tag.Title ?? Path.GetFileNameWithoutExtension(fileName);
                song.Length = String.Format(@"{0:mm\:ss}", fileID.Properties.Duration);
                song.URL = fileName;

                try { song.Artist = fileID.Tag.Performers[0]; } catch { song.Artist = "Unknown Artist"; }

                return song;
            }
            catch { return null; }
        }

        public ObservableCollection<Song> ScanAllSongs()
        {
            List<String> directories = CMP.Properties.Settings.Default.MusicDirectories;
            ObservableCollection<Song> songList = new ObservableCollection<Song>();

            try
            {
                if (directories != null)
                    foreach (String directory in directories)
                    {
                        if (Directory.Exists(directory))
                        {
                            var files = Directory.EnumerateFiles(directory, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".mp3") || s.EndsWith(".wav") || s.EndsWith(".wave") || s.EndsWith(".flac") || s.EndsWith(".aac") || s.EndsWith(".wma") || s.EndsWith("wmv"));

                            foreach (var file in files)
                            {
                                try
                                {
                                    TagLib.File fileID = TagLib.File.Create(file);
                                    Song song = new Song();

                                    song.Title = fileID.Tag.Title ?? Path.GetFileNameWithoutExtension(file);
                                    song.Length = String.Format(@"{0:mm\:ss}", fileID.Properties.Duration);
                                    song.URL = file;

                                    try
                                    {
                                        if (fileID.Tag.Performers.Length > 0)
                                            song.Artist = fileID.Tag.Performers[0];
                                        else
                                            song.Artist = "Unknown Artist";
                                    }
                                    catch (Exception artistException) { Console.WriteLine(artistException); }

                                    if (!songList.Contains(song))
                                    {
                                        songList.Add(song);
                                    }
                                }
                                catch { }
                            }
                        }
                    }
            }
            catch { }

            return songList;
        }
    }
}
