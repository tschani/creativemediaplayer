﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TagLib;
using System.Data.SQLite;
using System.IO;
using System.Windows;
using System.Threading;

namespace CMP
{
    public class Song
    {
        private String _title, _artist, _year, _album, _genre;
        private int _length;

        public String Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public String Artist
        {
            get { return _artist; }
            set { _artist = value; }
        }

        public String Year
        {
            get { return _year; }
            set { _year = value; }
        }

        public String Album
        {
            get { return _album; }
            set { _album = value; }
        }

        public String Genre
        {
            get { return _genre; }
            set { _genre = value; }
        }

        public int Length
        {
            get { return _length; }
            set { _length = value; }
        }
    }

    public class SongDB
    {
        private List<Song> songList = new List<Song>();
        

       
    }
}
