﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace CMP
{
    public partial class MusicFoldersPage : UserControl
    {
        public MusicFoldersPage()
        {
            InitializeComponent();

            if (CMP.Properties.Settings.Default.MusicDirectories != null)
                foreach (String f in CMP.Properties.Settings.Default.MusicDirectories)
                {
                    MusicFolderBox.Items.Add(f);
                }
        }

        private void RemoveFolder()
        {
            new Thread(() =>
            {
                try
                {
                    MusicFolderBox.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        if (MusicFolderBox.SelectedIndex >= 0)
                        {
                            CMP.Properties.Settings.Default.MusicDirectories.Remove(MusicFolderBox.SelectedValue.ToString());
                            CMP.Properties.Settings.Default.Save();

                            MusicFolderBox.Items.Remove(MusicFolderBox.SelectedItem);
                        }
                    }));
                }
                catch (Exception eX) { MessageBox.Show(eX.ToString()); }
            }).Start();
        }

        private void ContextMenuRemove(object sender, RoutedEventArgs e)
        {
            RemoveFolder();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.FolderBrowserDialog browserDialog = new System.Windows.Forms.FolderBrowserDialog();
                System.Windows.Forms.DialogResult result = browserDialog.ShowDialog();

                if (CMP.Properties.Settings.Default.MusicDirectories == null)
                    CMP.Properties.Settings.Default.MusicDirectories = new List<string>();

                if (!CMP.Properties.Settings.Default.MusicDirectories.Contains(browserDialog.SelectedPath) && browserDialog.SelectedPath.Length > 0)
                {
                    CMP.Properties.Settings.Default.MusicDirectories.Add(browserDialog.SelectedPath);
                    CMP.Properties.Settings.Default.Save();

                    MusicFolderBox.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        MusicFolderBox.Items.Add(browserDialog.SelectedPath);
                    }));
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }
    }
}