﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CMP
{
    public class SongList
    {

    }

    public class Song : INotifyPropertyChanged
    {
        private String _title, _artist, _year, _album, _genre, _length, _url;
        public event PropertyChangedEventHandler PropertyChanged;

        public Song(String Title = "", String Artist = "", String Year = "", String Album = "", String Genre = "", String Length = "", String URL = "", String FilePath = "")
        {
            this.Title = Title;
            this.Artist = Artist;
            this.Year = Year;
            this.Album = Album;
            this.Genre = Genre;
            this.Length = Length;
            this.URL = URL;
        }

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
                Console.WriteLine("WE CHANGED " + info);
            }
        }

        public String Title
        {
            get { return _title; }
            set { _title = value; NotifyPropertyChanged("Title"); }
        }

        public String Artist
        {
            get { return _artist; }
            set { _artist = value; NotifyPropertyChanged("Artist"); }
        }

        public String Year
        {
            get { return _year; }
            set { _year = value; NotifyPropertyChanged("Year"); }
        }

        public String Album
        {
            get { return _album; }
            set { _album = value; NotifyPropertyChanged("Album"); }
        }

        public String Genre
        {
            get { return _genre; }
            set { _genre = value; NotifyPropertyChanged("Genre"); }
        }

        public String Length
        {
            get { return _length; }
            set { _length = value; NotifyPropertyChanged("Length"); }
        }

        public String URL
        {
            get { return _url; }
            set { _url = value; NotifyPropertyChanged("URL"); }
        }
    }
}