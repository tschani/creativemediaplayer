﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CMP
{
    public partial class TagEditPage : UserControl
    {
        String metaURL = "";
        public TagEditPage(String url)
        {
            InitializeComponent();
            metaURL = url;

            TagLib.File metaFile = TagLib.File.Create(metaURL);

            //Load Metadata into TextBoxes
            titleBox.Text = metaFile.Tag.Title;
            if (metaFile.Tag.Performers.Length > 0)
                artistBox.Text = metaFile.Tag.Performers[0];
        }
    }
}
